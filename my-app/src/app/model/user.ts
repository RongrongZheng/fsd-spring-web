export class User { 
  id: number;
  fullname: string; 
  username: string;  
  password: string; 
  email: string;
  constructor(id,username,password,email,fullname){
    this.id=id;
    this.username=username;
    this.password=password;
    this.email=email;
    this.fullname=fullname;

  }

}