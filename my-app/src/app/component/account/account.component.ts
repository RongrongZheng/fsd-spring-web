import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from './../../core/users.service'
import {  User } from '../../model/user'
import { NzModalService } from 'ng-zorro-antd/modal';
import {Router} from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    console.log("===>>>start  update user<<<====")
    
    let user= new User (
      this.validateForm.controls["id"].value,
      this.validateForm.controls["username"].value,
      this.validateForm.controls["password"].value,
      this.validateForm.controls["email"].value,
      this.validateForm.controls["fullname"].value
      
    )
    console.log("save the user:"+this.validateForm.controls["username"].value)
    this.usersService.updateUser(user)
    .subscribe(
      data =>{ 
        if(data.status == "1"){
          console.log(data)
          const modal = this.modalService.success({
            nzTitle: 'tip',
            nzContent: 'update successfully'
          });       
          setTimeout(() => modal.destroy(), 2000);
          this.router.navigate([`homepage`])
        }
      }
    );
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }

  constructor(private fb: FormBuilder,
    private usersService: UsersService,
    private modalService: NzModalService,
    private router: Router) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      id:[null, [ Validators.required]],
      fullname:[null, [ Validators.required]],
      username:[null, [ Validators.required]],
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]]
    });
  }

}
