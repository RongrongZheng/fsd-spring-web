import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import{ LoginService } from './../../core/login.service'
import{ User } from './../../model/user'
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.css']
})
export class LoginLayoutComponent implements OnInit {

  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }    
   console.log("tstsssss")
    this.loginService.login(this.validateForm.controls["username"].value,this.validateForm.controls["password"].value)
    .subscribe(
      data =>{ 
        console.log(data)
        if(data.status == "1"){this.router.navigate([`homepage`]);}
        else {}
      }
    );

  }
  resetForm(): void {
    this.validateForm.reset();
  }

  

  constructor(private fb: FormBuilder,
          private loginService: LoginService,
          private router: Router
    ) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
}
