import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';

interface ItemData {
  name: string;
  age: number;
  address: string;
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  listOfData: User[] = [];

  ngOnInit(): void {
    for (let i = 0; i < 10; i++) {
      let user = new User( i,`test${i}`,'password',`test${i}@cn.test.com`,`Jack ${i}`)
      this.listOfData.push(user);
    }
  }

}
