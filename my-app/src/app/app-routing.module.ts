import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginLayoutComponent } from './component/login-layout/login-layout.component';
import { HomeComponent } from './component/home/home.component';
import { UserComponent } from './component/user/user.component';
import { AdminComponent } from './component/admin/admin.component';
import { TutorialsComponent } from './component/tutorials/tutorials.component';
import { AccountComponent } from './component/account/account.component';

const routes: Routes = [
  { path: 'login', component: LoginLayoutComponent},
  { path: 'logout', component: LoginLayoutComponent},
  { 
    path: 'homepage', 
    component: HomeComponent,
    children:[
      { path: 'admin', component: AdminComponent },
      { path: 'tutorials', component: TutorialsComponent },
      { path: 'accountupdate', component: AccountComponent },
      { path:  '**', component: AdminComponent}
     
    ]
  },
  { path:  '**', component: HomeComponent}
];

@NgModule({
  imports: [
      RouterModule.forRoot(
          routes,
          { enableTracing: true } // <-- debugging purposes only
        )
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }




