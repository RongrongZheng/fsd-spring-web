import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from './../core/http-error-handler.service';
import { HttpService } from './http.service';

import { User } from './../model/user'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class LoginService {
  //loginUrl = "api/spring-mvc/login/user";  // URL to web api
  loginUrl = "api/spring-mvc/login"; 
  private handleError: HandleError;

  constructor(
    private http: HttpService,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UseresService');
  }

  
  /** POST: add a new hero to the database */
  loginDefHeader (username: String, password: String): Observable<Object> {
    const httpOptions = {
      headers : new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded' })
    }
    let data ={'username':username, 'password':password}
    let ret = ''
    for (let it in data) {
      ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
    }  

    //this.http.get(this.loginUrl)
    return this.http.postAddOption(this.loginUrl,ret,httpOptions) as Observable<any> ;

  }

  login(username: String, password: String): Observable<any> {

    return this.http.post(this.loginUrl,{'name':username,'password':password}) as Observable<any>;
  }

  sendCredential(username: string, password: string){
    
    return this.http.sendCredential(username,password);
  }

}
