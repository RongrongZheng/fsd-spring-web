import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from './../core/http-error-handler.service';
import { HttpService } from './http.service';

import { User } from './../model/user'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class UsersService {
  UsersUrl = "api/spring-mvc/user";  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpService,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UseresService');
  }

  /** GET heroes from the server */
  getUsers (): Observable<User[]> {
    return this.http.get(this.UsersUrl) as Observable<User[]> ;
  }
  searchUser(id: number): Observable<User> {
    const url = this.UsersUrl+"/"+id;
    return  this.http.get(url);
  }
  /** POST: add a new hero to the database */
  addUser (user: User): Observable<User> {
    return this.http.post(this.UsersUrl, user) as Observable<any> ;
  }

  /** DELETE: delete the hero from the server */
  deleteUser (id: number): Observable<{}> {
    const url = `${this.UsersUrl}/${id}`; // DELETE api/heroes/42
    return this.http.delete(url);
  }

  /** PUT: update the hero on the server. Returns the updated hero upon success. */
  updateUser (user: User): Observable<any> {
    console.log("=====user:"+user.username)
    return this.http.post(this.UsersUrl, user) as Observable<any>;
  }
}
