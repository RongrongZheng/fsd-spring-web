import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './component/user/user.component';
import { LoginLayoutComponent } from './component/login-layout/login-layout.component';
import { HomeComponent } from './component/home/home.component';
import { TestComponent } from './component/test/test.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { UsersService } from './core/users.service';
import { LoginService } from './core/login.service';
import { HttpService } from './core/http.service';
import { HttpErrorHandler } from './core/http-error-handler.service';
import { MessageService } from './core/message.service';
import { AccountComponent } from './component/account/account.component';
import { AdminComponent } from './component/admin/admin.component';
import { TutorialsComponent } from './component/tutorials/tutorials.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginLayoutComponent,
    HomeComponent,
    TestComponent,
    AccountComponent,
    AdminComponent,
    TutorialsComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    UsersService,LoginService,HttpService,HttpErrorHandler,MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
